import classes.Apartment;

public class App {
    public static void main(String[] args) {
        Apartment apartment1 = new Apartment(11, 8, 110.f, "Ponta Verde", null);
        System.out.println("Informações do apartamento:");
        System.out.println("Número de pavimentos: " + apartment1.getPavements());
        System.out.println("Número de cômodos: " + apartment1.getRooms());
        System.out.println("Área construída: " + apartment1.getArea() + " metros quadrados");
        System.out.println("Localização: " + apartment1.getLocalization());
        apartment1.sell();
        System.out.println("Disponibilidade: " + apartment1.getAvailability());
        apartment1.saleInformation();

        System.out.println("");

        Apartment apartment2 = new Apartment(7, 8, 78.f, "Jatiúca", null);
        System.out.println("Informações do apartamento:");
        System.out.println("Número de pavimentos: " + apartment2.getPavements());
        System.out.println("Número de cômodos: " + apartment2.getRooms());
        System.out.println("Área construída: " + apartment2.getArea() + " metros quadrados");
        System.out.println("Localização: " + apartment2.getLocalization());
        apartment2.rent();
        System.out.println("Disponibilidade: " + apartment2.getAvailability());
        apartment2.rentInformation();

        System.out.println("");

        Apartment apartment3 = new Apartment(12, 7, 62.f, "Pajuçara", null);
        System.out.println("Informações do apartamento:");
        System.out.println("Número de pavimentos: " + apartment3.getPavements());
        System.out.println("Número de cômodos: " + apartment3.getRooms());
        System.out.println("Área construída: " + apartment3.getArea() + " metros quadrados");
        System.out.println("Localização: " + apartment3.getLocalization());
        apartment3.reform();
        System.out.println("Disponibilidade: " + apartment3.getAvailability());
        apartment3.reformInformation();
    }
}
